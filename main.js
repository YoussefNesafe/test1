var ishow = document.getElementById('#show');

ishow.onclick = function () {
  ishow.nextElementSibling.classList.toggle('showhide');
}

//Select All links in The navigation bar
const allLinks = document.querySelectorAll(".links a");

//Select All Bullets
const allBullets = document.querySelectorAll(".nav-bullets .bullet");

//Function for smooth scroll 
function scroolToSomeWhere(elements) {
  elements.forEach((ele) => {
    ele.addEventListener("click", (e) => {
      e.preventDefault();
      document.querySelector(e.target.dataset.section).scrollIntoView({
        behavior: "smooth",
      });
    });
  });
}
scroolToSomeWhere(allLinks);
scroolToSomeWhere(allBullets);


//To Top Button
const backToTop = document.querySelector('#toTop');

window.addEventListener("scroll", scrollFunction);

function scrollFunction() {
  if ((window.pageYOffset > 300) && (window.innerWidth < 571)) { //Show Back To Top button
    if (!backToTop.classList.contains('zoomIn')) {
      backToTop.classList.remove('zoomOut');
      backToTop.classList.add('zoomIn');
      backToTop.style.display = "block";
    }
  }
  else { // Hide Back To Top button
    if (!backToTop.classList.contains('zoomOut')) {
      backToTop.classList.remove('zoomIn');
      backToTop.classList.add('zoomOut');
      setTimeout(function () {
        backToTop.style.display = "none";
      }, 1000);
    }
  }
}
backToTop.onclick = function () {
  window.scrollTo({
    left: 0,
    top: 0,
    behavior: "smooth"
  });
}